package com.mashibing.juc.c_001_03_Ordering;

public class T02_NoVisibility {
    //1.可见性问题：加volatile
    private static boolean ready = false;
    private static int number;

    private static class ReaderThread extends Thread {
        @Override
        public void run() {
            while (!ready) {
                Thread.yield();
            }
            //2.有序性问题：有可能number输出的是0，因为乱序
            System.out.println(number);
        }
    }

    public static void main(String[] args) throws Exception {
        Thread t = new ReaderThread();
        t.start();
        number = 42;
        ready = true;
        t.join();
    }
}
