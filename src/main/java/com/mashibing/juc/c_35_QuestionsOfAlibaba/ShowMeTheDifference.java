package com.mashibing.juc.c_35_QuestionsOfAlibaba;

public class ShowMeTheDifference {
}

//4.指出以下两段程序的差别，并分析

final class Accumulator {
    private double result = 0.0D;

    public void addAll(double[] values) {
        for (double value : values) {
            result += value;
        }
    }
}

final class Accumulator2 {
    private double result = 0.0D;

    public void addAll(double[] values) {
        double sum = 0.0D;
        for (double value : values) {
            sum += value;
        }
        result += sum;
    }
}
