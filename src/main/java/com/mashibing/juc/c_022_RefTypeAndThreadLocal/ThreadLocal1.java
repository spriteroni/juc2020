/**
 * ThreadLocal�ֲ߳̾�����
 *
 * @author ��ʿ��
 */
package com.mashibing.juc.c_022_RefTypeAndThreadLocal;

import com.mashibing.util.SleepHelper;

import java.util.concurrent.TimeUnit;

public class ThreadLocal1 {
    static Person p = new Person();

    public static void main(String[] args) {

        new Thread(() -> {
            SleepHelper.sleepSeconds(1);
            p.name = "lisi";
        }).start();

        new Thread(() -> {
            SleepHelper.sleepSeconds(2);
            System.out.println(p.name);
        }).start();

    }
}

class Person {
    String name = "zhangsan";
}
