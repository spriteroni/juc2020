/**
 * 弱引用遭到gc就会回收
 */
package com.mashibing.juc.c_022_RefTypeAndThreadLocal;

import com.mashibing.util.SleepHelper;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public class T03_WeakReference {
    public static void main(String[] args) {
        WeakReference<M> wr = new WeakReference<>(new M());

        System.out.println(wr.get());
        System.gc();
        SleepHelper.sleepSeconds(1);
        System.out.println(wr.get());




        /*ThreadLocal<M> tl = new ThreadLocal<>();
        tl.set(new M());
        tl.remove();*/

    }
}

