package com.mashibing.insidesync;

import com.mashibing.util.SleepHelper;
import org.openjdk.jol.info.ClassLayout;

import java.util.Hashtable;
import java.util.Vector;

public class T01_Sync1 {

    private static class T {

    }

    public static void main(String[] args) {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        T t = new T();
        System.out.println(ClassLayout.parseInstance(t).toPrintable());

        synchronized (t) {
            System.out.println(ClassLayout.parseInstance(t).toPrintable());
        }

        System.out.println(ClassLayout.parseInstance(t).toPrintable());

        Vector v = new Vector();
        v.add(new Object());
        System.out.println();
    }
}
