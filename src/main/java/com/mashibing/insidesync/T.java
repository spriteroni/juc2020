package com.mashibing.insidesync;

import org.openjdk.jol.info.ClassLayout;

public class T {
    private static Object o;


    public static void main(String[] args) {


        o = new Object();

        //JOL = java object layout
        System.out.println(ClassLayout.parseInstance(o).toPrintable());

        synchronized (o) {
            System.out.println(ClassLayout.parseInstance(o).toPrintable());
        }

        System.out.println(ClassLayout.parseInstance(o).toPrintable());


    }


}
