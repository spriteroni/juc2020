package com.mashibing;

import com.mashibing.juc.c_026_01_ThreadPool.T07_SingleThreadPool;
import com.mashibing.util.SleepHelper;
import org.openjdk.jol.info.ClassLayout;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

public class JustTest { //JOL = Java Object Layout //OpenJDK


    public static void main(String[] args) {

        Object obj = new Object();
        System.out.println(ClassLayout.parseInstance(obj).toPrintable());

        synchronized (obj) {
            System.out.println(ClassLayout.parseInstance(obj).toPrintable());
            try {
                obj.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}



